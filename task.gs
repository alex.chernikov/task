var sheetOne = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
sheetOne.getDataRange().clear();

var data = [["Group name", "User firstname", "User lastname", "User email"]];
rowInsert(data);

function main() {
  var groups = AdminDirectory.Groups.list({
      domain: 'chucknorris.fun'}).groups;
    
  for (var i = 0; i < groups.length; i++) {
    var group = GroupsApp.getGroupByEmail(groups[i].email);
    groupProcessing(group);
  } 
}

function groupProcessing(group) {
  var group_info = AdminGroupsSettings.Groups.get(group.getEmail());
  var users = group.getUsers();
  for (var i = 0; i < users.length; i++) {
    var user_info =  AdminDirectory.Users.get(users[i].getEmail());
    var data = [[group_info.name, user_info.name.givenName, user_info.name.familyName, user_info.primaryEmail]];
    rowInsert(data);
  }
}

function rowInsert(data) {
  row = sheetOne.getLastRow() + 1;
  sheetOne.getRange(row,1,1,data[0].length).setValues(data);
}
